package com.jairek.zkossapp.viewModel;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;

import com.jairek.zkossapp.model.Album;
import com.jairek.zkossapp.model.Person;
import com.jairek.zkossapp.service.AuthService;
import com.jairek.zkossapp.service.EntityManager;

public class AlbumVM {

	ListModelList<Album> albums;//the album list
    Album selected;//the selected album
    EntityManager em = new EntityManager();
    Person currentUser = AuthService.getCurrentUser();
    
	public ListModelList<Album> getAlbums() {
        if (albums == null) {
            albums = new ListModelList<Album>(em.getAlbumsForPerson(AuthService.getCurrentUser().getId()));
        }
        return albums;
    }
 
    @NotifyChange("selected")
    public void setSelected(Album selected) {
        this.selected = selected;
    }
 
    @Command @NotifyChange({"selected","albums"})
    public void newAlbum(){
        Album album = new Album();
        selected = album;
    }
     
    @Command 
    @NotifyChange({"selected","notice"})
    public void saveAlbum(){
    	try {
    		if (selected.getId() > 0) {
    			Album updateAlbum = em.editAlbum(selected, currentUser);
    			selected.setName(updateAlbum.getName());
    		} else {
    			Album newAlbum = em.addAlbum(selected, currentUser);
    			getAlbums().add(newAlbum);
    			selected = newAlbum;
    		}
		} catch (Exception e) {
			Clients.showNotification(e.getMessage(), "error", null, "middle_center", 300);
			e.printStackTrace();
		}
    }
    
    @Command 
    @NotifyChange({"selected","notice"})
    public void deleteAlbum(){
    	int count = em.deleteAlbum(selected, currentUser);
    	if (count > 0) {
        	albums.remove(selected);
        	selected = null;
    	}
    }

	public Album getSelected() {
		if (selected == null) {
			selected = new Album();
		}
		return selected;
	}

	public void setAlbums(ListModelList<Album> albums) {
		this.albums = albums;
	}
	
	public Validator getNameValidator(){
	    return new AbstractValidator(){
	        public void validate(ValidationContext ctx) {
	           String name = (String)ctx.getProperty().getValue();
	           if(name == null || !(name.length() > 0) ){
	        	   addInvalidMessage(ctx, "Name must not be empty");
	           }
		   		if (em.getAlbumByName(name, currentUser.getId()) != null) {
		   			addInvalidMessage(ctx, "Album already exist");
				}
	        }
	    };
	}
	
}
