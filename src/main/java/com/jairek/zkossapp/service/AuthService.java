package com.jairek.zkossapp.service;

import com.jairek.zkossapp.model.*;
import org.zkoss.zk.ui.Sessions;

public class AuthService {
	private EntityManager em = new EntityManager();
	public AuthService() {
		
	}
	
	public String register(String login, String password) {
		Person p = new Person();
		p.setName(login);
		p.setPassword(password);	
		em.addPerson(p);
		p = em.getPerson(login, password);
		if (em != null) {
			return p.getName(); 
		}
		return null;
	}

	
	public Person login(String login, String password) throws Exception {
		Person p;
		p = em.getPersonByNick(login);
		if (p != null) {
			if (p.getPassword().equals(password)) {
				return p;
			} else {
				throw new Exception("Login failed!");
			}
		} else {
			p = new Person();
			p.setName(login);
			p.setPassword(password);	
			if (em.addPerson(p) > 0 ) {
				p = em.getPersonByNick(login);
				Album al = new Album();
				al.setName("default");
				al.setUserId(p.getId());
				em.addAlbum(al, p);
				return p;
			} else {
				throw new Exception("Register failed!");
			}	
		}
	}
	
	public static boolean isAuthenticated() {
		if ((getCurrentUser() != null)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static Person getCurrentUser() {
		EntityManager em = new EntityManager();
		Person session_user = (Person) Sessions.getCurrent().getAttribute("current_user");
		if (session_user == null) {
			return null;
			//return em.getPersonByNick("jairek");
		} else {			
			return em.getPersonByNick(session_user.getName());
		}	
	}
}