package com.jairek.zkossapp.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;


public class AuthenticateInit extends org.zkoss.zk.ui.util.GenericInitiator {
    public void doInit(Page page, Map args) throws Exception {
        if (!AuthService.isAuthenticated()) {
            Execution exec = Executions.getCurrent();
            HttpServletResponse response = (HttpServletResponse)exec.getNativeResponse();
            response.sendRedirect(response.encodeRedirectURL("/zkossapp/login.zul"));
            exec.setVoided(true); 
        }
    }
}
