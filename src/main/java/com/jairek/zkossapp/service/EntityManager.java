package com.jairek.zkossapp.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jairek.zkossapp.model.*;

public class EntityManager {

	Connection connection     = null;
    String url          = "jdbc:mysql://localhost:3306/";
    String db           = "gallery";
    String driver       = "com.mysql.jdbc.Driver";
    String user         = "root";
    String pass         = "admin";

	private String createTablePerson = "CREATE TABLE  `person` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`nick` VARCHAR( 25 ) NOT NULL , `password` VARCHAR( 25 ) NOT NULL) ";
	private String createTableAlbum = "CREATE TABLE  `album` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY , `name` VARCHAR( 25 ) NOT NULL , `user_id` INT NOT NULL) ";
	private String createTablePhoto = "CREATE TABLE  `photo` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY , `name` VARCHAR( 255 ) NOT NULL , `path` VARCHAR( 255 ) NOT NULL , `description` VARCHAR( 255 ) NULL , `album_id` INT NOT NULL)";
	
	
	private Statement statement;
	
	private PreparedStatement getPersonStmt;
	private PreparedStatement addPersonStmt;
	private PreparedStatement getPersonByNickStmt;
	private PreparedStatement addAlbumStmt;
	private PreparedStatement getAlbumStmt;
	private PreparedStatement getAlbumsForPersonStmt;
	private PreparedStatement addPhotoStmt;
	private PreparedStatement updateAlbumStmt;
	private PreparedStatement deleteAlbumStmt;
	private PreparedStatement getPhotosWithAlbumStmt;
	private PreparedStatement getAlbumByIdStmt;
	
	public EntityManager() {
		try {
			 Class.forName(driver).newInstance();
			connection = DriverManager.getConnection(url+db, user, pass);
			statement = connection.createStatement();
			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tablePersonExists = false;
			boolean tablePhotoExists = false;
			boolean tableAlbumExists = false;
			while (rs.next()) {
				if ("Person".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tablePersonExists = true;
				} else if ("Album".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableAlbumExists = true;
				} else if ("Photo".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tablePhotoExists = true;
				}
			}

			if (!tablePersonExists) statement.executeUpdate(createTablePerson);
			if (!tableAlbumExists) statement.executeUpdate(createTableAlbum);
			if (!tablePhotoExists) statement.executeUpdate(createTablePhoto);
			
			getPersonStmt = connection.prepareStatement("SELECT * FROM  `person` WHERE  `nick` =  ? AND  `password` =  ?");
			getPersonByNickStmt = connection.prepareStatement("SELECT * FROM  `person` WHERE  `nick` =  ?");
			addPersonStmt = connection.prepareStatement("INSERT INTO `person` (`nick` ,`password`) VALUES (?, ?)");
			addAlbumStmt = connection.prepareStatement("INSERT INTO `album` (`name`, `user_id`) VALUES (?, ?)");
			updateAlbumStmt = connection.prepareStatement("UPDATE `album` SET `name`= ? WHERE `id` = ? AND `user_id`= ?");
			deleteAlbumStmt = connection.prepareStatement("DELETE FROM `album` WHERE `id` = ? AND `user_id`= ?");
			addPhotoStmt = connection.prepareStatement("INSERT INTO `photo` (`name` ,`path`, `description`,`album_id`) VALUES (?, ?, ?, ?)");
			getAlbumStmt =  connection.prepareStatement("SELECT * FROM  `album` WHERE  `name` =  ? AND `user_id` = ?");
			getAlbumByIdStmt =  connection.prepareStatement("SELECT * FROM  `album` WHERE  `id` =  ? AND `user_id` = ?");
			getAlbumsForPersonStmt = connection.prepareStatement("SELECT * from `album` where `user_id` = ?");
			getPhotosWithAlbumStmt = connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? ORDER BY  `photo`.`id` ASC");
	} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}
	
	public Person getPerson(String name, String password) {
		Person p = new Person();
		
		try {
			getPersonStmt.setString(1, name);
			getPersonStmt.setString(2, password);
			
			ResultSet rs = getPersonStmt.executeQuery();
			if (rs.next() ) {
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("nick"));
				p.setPassword(rs.getString("password"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return p;
	}
	
	public Person getPersonByNick(String nick) {
		Person p = new Person();
		
		try {
			getPersonByNickStmt.setString(1, nick);
			
			ResultSet rs = getPersonByNickStmt.executeQuery();
			if (rs.next() ) {
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("nick"));
				p.setPassword(rs.getString("password"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return p;
	}
	
	public int addPerson(Person person) {
		int count = 0;
		try {
			addPersonStmt.setString(1, person.getName());
			addPersonStmt.setString(2, person.getPassword());

			count = addPersonStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public Album addAlbum(Album album, Person p) throws Exception {
		if (album.getName() == null || !(album.getName().length() > 0)){
			throw new Exception("Name must not be empty");
		}
		if (getAlbumByName(album.getName(), p.getId()) != null) {
			throw new Exception("Album already exist");
		} 

		int count = 0;
		try {
			addAlbumStmt.setString(1, album.getName());
			addAlbumStmt.setInt(2, p.getId());

			count = addAlbumStmt.executeUpdate();
			if (count > 0 ) {
				return getAlbumByName(album.getName(), p.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Album editAlbum(Album album, Person p) throws Exception {
		if (album.getName() == null || !(album.getName().length() > 0)){
			throw new Exception("Name must not be empty");
		}
		if (getAlbumByName(album.getName(), p.getId()) != null) {
			throw new Exception("Album already exist");
		}
		int count = 0;
		try {
			updateAlbumStmt.setString(1, album.getName());
			updateAlbumStmt.setInt(2, album.getId());
			updateAlbumStmt.setInt(3, p.getId());

			count = updateAlbumStmt.executeUpdate();
			if (count > 0 ) {
				return getAlbumByName(album.getName(), p.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public int deleteAlbum(Album album, Person p) {
		int count = 0;
		try {
			deleteAlbumStmt.setInt(1, album.getId());
			deleteAlbumStmt.setInt(2, p.getId());

			count = deleteAlbumStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;	
	}
	
	public Album getAlbumByName(String name, int user_id) {
		Album a = new Album();
		try {
			getAlbumStmt.setString(1, name);
			getAlbumStmt.setInt(2, user_id);
			
			ResultSet rs = getAlbumStmt.executeQuery();
			if (rs.next() ) {
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setUserId(rs.getInt("user_id"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return a;
	}
	
	public Album getAlbumById(int id, int user_id) {
		Album a = new Album();
		try {
			getAlbumByIdStmt.setInt(1, id);
			getAlbumByIdStmt.setInt(2, user_id);			
			ResultSet rs = getAlbumByIdStmt.executeQuery();
			if (rs.next() ) {
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setUserId(rs.getInt("user_id"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return a;
	}
	
	public List<Album> getAlbumsForPerson(int user_id) {
		List<Album> albums = new ArrayList<Album>();
		try {
			getAlbumsForPersonStmt.setInt(1, user_id);
			ResultSet rs = getAlbumsForPersonStmt.executeQuery();

			while (rs.next()) {
				Album a = new Album();
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setUserId(rs.getInt("user_id"));
				albums.add(a);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return albums;
	}
	
	public int addPhoto(Photo p) {
		int count = 0;
		try {
			addPhotoStmt.setString(1, p.getName());
			addPhotoStmt.setString(2, p.getPath());
			addPhotoStmt.setString(3, p.getDescription());
			addPhotoStmt.setInt(4, p.getAlbumId());

			count = addPhotoStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public ArrayList<Photo> getPhotosWithAlbum(int album_id, int user_id) {
		ArrayList<Photo> photos = new ArrayList<Photo>();
		try {
			getPhotosWithAlbumStmt.setInt(2, user_id);
			getPhotosWithAlbumStmt.setInt(1, album_id);
			ResultSet rs = getPhotosWithAlbumStmt.executeQuery();

			while (rs.next()) {
				Photo a = new Photo();
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setAlbumId(rs.getInt("album_id"));
				a.setPath(rs.getString("path"));
				a.setDescription(rs.getString("description"));
				photos.add(a);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return photos;
	}
	
}
