package com.jairek.zkossapp.model;

import java.io.Serializable;

public class Photo implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String path;
	private String description;
	private int albumId;
	
	public Photo(){
		this.name ="";
		this.path="";
		this.description="";
	}
	public Photo(String name, String path, String description, Album album){
		this.name = name;
		this.path=path;
		this.description=description;
		this.albumId=album.getId();
	}
	
	public int getAlbumId() {
		return albumId;
	}
	public void setAlbumId(int album_id) {
		this.albumId = album_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
