package com.jairek.zkossapp.model;

import java.io.Serializable;

public class Album implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private int user_id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	public String toString(){
		return "To_string" + this.name;
	}
	public String getUrl(){
		return "showAlbum.zul?id=" + id;
	}

}
