package com.jairek.zkossapp.model;

public enum WhichPhoto {
	FIRST, LAST, NEXT, PREV, RANDOM
}
