package com.jairek.zkossapp.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Selectbox;
import org.zkoss.zul.Textbox;

import com.jairek.zkossapp.model.*;
import com.jairek.zkossapp.service.*;

public class PhotoController  extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;
	private EntityManager em = new EntityManager();
	private static final String SAVE_DIRECTORY = "src" + File.separator + "main" + File.separator + "webapp" + File.separator;
	private static final String UPLOAD_DIRECTORY = "uploads" + File.separator;
	private ListModel<Album> albums = new ListModelList<Album>(em.getAlbumsForPerson(AuthService.getCurrentUser().getId()));
	org.zkoss.util.media.Media media;
	private Photo photo;
	
	@Wire
	private Fileupload photoUpload;
	@Wire 
	private Textbox description;
	@Wire 
	private Textbox name;	
	@Wire 
	private Selectbox album;	
	@Wire
	private Button submit;	
	@Wire
	private Label nameValidation;
	@Wire
	private Label albumValidation;	
	@Wire
	private Label photoUploadValidation;

	public PhotoController(){
		photo = new Photo();
	}

	@Listen("onBlur = #name")
	public void onBlurName () {
		validateName();
	}
	@Listen("onBlur = #album")
	public void onBlurAlbum () {
		validateAlbum();
	}
	
	@Listen("onClick = #submit")
	public void submit () {
		if (validateForm()) {
			Set<Album> selectedAlbums = ((ListModelList<Album>)albums).getSelection();
		    Album selectedAlbum = selectedAlbums.iterator().next();
		    photo.setName(name.getValue());
		    photo.setDescription(description.getValue());
		    photo.setAlbumId(selectedAlbum.getId());	
		    String path = saveImage(media);
		    photo.setPath(path);
		    if (em.addPhoto(photo) > 0) {
			    clearForm();
			    Clients.showNotification("Photo was added", "info", null, "middle_center", 300);	
		    } else {
		    	Clients.showNotification("Something wrong", "error", null, "middle_center", 300);	
		    }
		}
	}
	
	@Listen("onUpload = #photoUpload")
	public void upload(org.zkoss.zk.ui.event.UploadEvent ue) {
		media = ue.getMedia();
		validatePhoto();
	}
	
	public void clearForm() {
		name.setValue("");
		description.setValue("");
		((ListModelList<Album>)albums).clearSelection();
		media = null;
		photo = new Photo();
	}
	
	private Album getSelectedAlbum() {
		Set<Album> selectedAlbums = ((ListModelList<Album>)albums).getSelection();
	    return selectedAlbums.iterator().next();
	}
	
	public String saveImage(Media media) {
		try {		
			if(media == null){
				return null;
			}	
			
			String direcotry_path = UPLOAD_DIRECTORY + File.separator + AuthService.getCurrentUser().getId() + File.separator + getSelectedAlbum().getId();
			File tempDir = new File(SAVE_DIRECTORY + direcotry_path);
			if (!tempDir.exists()) {
				tempDir.mkdirs();
			}
			String image_path = ""+UUID.randomUUID()+".jpg";
			File f= new File(SAVE_DIRECTORY+ direcotry_path + File.separator, image_path);
			
	        InputStream inputStream = media.getStreamData();
	        OutputStream out = new FileOutputStream(f);	
			
	        byte buf[]=new byte[1024];
	        int len;	       
			
	        while((len=inputStream.read(buf))>0)
	        	out.write(buf,0,len);
			
	        out.close();			
	        inputStream.close();
	        
	        return direcotry_path + File.separator + image_path;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ListModel<Album> getAlbums() {
		return albums;
	}

	/* ------------------- Validatory --------------------------- */
	public boolean validateName() {
		boolean check = true;
		if(name == null || !(name.getValue().length() > 0) ){
			nameValidation.setValue("Name may not be empty");
			check = false;
        } else {
        	nameValidation.setValue("");
        }
		return check;
	}
	
	public boolean validateAlbum() {
		boolean check = true;
		Set<Album> selectedAlbums = ((ListModelList<Album>)albums).getSelection();
		if (selectedAlbums.isEmpty()) {
			albumValidation.setValue("Album may not be empty");
			check = false;
		} else {
			albumValidation.setValue("");
		}
		return check;
	}
	
	public boolean validatePhoto() {
		boolean check = true;
		if (media == null) {
			photoUploadValidation.setValue("Photo may not be empty");
			check = false;
		} else {
			photoUploadValidation.setValue("");
		}
		return check;
	}
	
	public boolean validateForm() {
		boolean check = true;
		if (!validateAlbum()) check = false;
		if (!validateName()) check = false;
		if (!validatePhoto()) check = false;
		
		return check;
	}
	
}
