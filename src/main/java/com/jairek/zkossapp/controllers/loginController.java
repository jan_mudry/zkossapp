package com.jairek.zkossapp.controllers;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;

import com.jairek.zkossapp.model.Person;
import com.jairek.zkossapp.service.*;

public class loginController  extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;
	private AuthService authService = new AuthService();
	private Person user;
	
	@Wire
    private Button submitButton; 
	@Wire
	private Textbox nameBox, passwordBox;
	
	public loginController(){
		if (AuthService.isAuthenticated()) {
			Executions.sendRedirect("index.zul");
		}
	}
	
	@Listen("onClick = #submitButton")
	public void login(){
		try {
			if (nameBox.getValue() != null || nameBox.getValue() != ""){
				user = authService.login(nameBox.getValue(), passwordBox.getValue());
				if (user != null) { 
					Sessions.getCurrent().setAttribute("current_user", user);
				}
			}
		} catch (Exception e) {
			Clients.showNotification(e.getMessage(), "warning", null, "middle_center", 300);
		}
		
		Person current_user = (Person) Sessions.getCurrent().getAttribute("current_user");
		if (current_user != null) {
			Executions.sendRedirect("index.zul");
		} 		
	}
}
