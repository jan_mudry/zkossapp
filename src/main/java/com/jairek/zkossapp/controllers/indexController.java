package com.jairek.zkossapp.controllers;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import com.jairek.zkossapp.service.*;

public class indexController  extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	@Wire
    private Button sesionLogout; 	
	
	@Listen("onClick = #sesionLogout")
	public void logout(){
		Sessions.getCurrent().removeAttribute("current_user");
		Executions.sendRedirect("login.zul");
	}
	
	public String getUserName() {
		return AuthService.getCurrentUser().getName();
	}
	
}
