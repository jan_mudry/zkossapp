package com.jairek.zkossapp.controllers;

import java.util.ArrayList;
import java.util.Random;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import com.jairek.zkossapp.model.*;
import com.jairek.zkossapp.service.*;

public class AlbumController  extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;
	private EntityManager em = new EntityManager();

	@Wire
	private Label albumNameLabel;
	@Wire
	private Div imageContainer;
	@Wire
	private Button firstButton;
	@Wire
	private Button prevButton;
	@Wire
	private Button randomButton;
	@Wire
	private Button nextButton;
	@Wire
	private Button lastButton;
	@Wire
	private Image currentImage;
	@Wire
	private Label description;
	
	private Photo currentPhoto;
	int current_photo_id;
	ArrayList<Photo> photos;
	int count_photos;
	
	Album album;

	boolean getAuthenticated(){
		return AuthService.isAuthenticated(); 
	}

	public AlbumController(){
		int album_id;
		if (Executions.getCurrent().getParameter("id") != null) {
			try {
				album_id = Integer.parseInt(Executions.getCurrent().getParameter("id"));
				album = em.getAlbumById(album_id, AuthService.getCurrentUser().getId());
			} catch (Exception e){
				e.printStackTrace();				
				album = null;
			} 
			if (album == null) {
				Executions.sendRedirect("404.html");
				return;
			}
			photos = em.getPhotosWithAlbum(album.getId(),AuthService.getCurrentUser().getId());
			count_photos = photos.size();
			if (!photos.isEmpty()) {				
				changePhoto(WhichPhoto.FIRST);
			}
		}
	}
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		if (photos != null && !photos.isEmpty()) {				
			changePhoto(WhichPhoto.FIRST);
		}		
	}
	
	@Listen("onClick = #firstButton")
	public void firstButtonClick () {
		changePhoto(WhichPhoto.FIRST);
	}
	
	@Listen("onClick = #prevButton")
	public void prevButtonClick () {
		changePhoto(WhichPhoto.PREV);
	}
	
	@Listen("onClick = #randomButton")
	public void randomButtonClick () {
		changePhoto(WhichPhoto.RANDOM);
	}
	
	@Listen("onClick = #nextButton")
	public void nextButtonClick () {
		changePhoto(WhichPhoto.NEXT);
	}
	
	@Listen("onClick = #lastButton")
	public void lastButtonClick () {
		changePhoto(WhichPhoto.LAST);
	}
	
	@Listen("onMouseOver = #imageContainer")
	public void descriptionOnMouseOver () {
		System.out.println("onMouseOver");
		description.setVisible(true);
	}
	
	@Listen("onMouseOut = #imageContainer")
	public void descriptionOnMouseOut () {
		System.out.println("onMouseOut");
		description.setVisible(false);
	}
	
	public void changePhoto(WhichPhoto which) {
		switch (which) {
		case FIRST:
			current_photo_id = 0;
			break;
		case LAST:
			current_photo_id = count_photos -1;
			break;
		case NEXT:
			if (current_photo_id < count_photos-1) {
				current_photo_id++;
			}
			break;
		case PREV:
			if (current_photo_id > 0) {
				current_photo_id--;
			}
			break;
		case RANDOM:
			int randomInt;
			do {
				randomInt = new Random().nextInt(count_photos);
			} while (randomInt == current_photo_id);
			current_photo_id = randomInt;
			break;
		}
		currentPhoto = photos.get(current_photo_id);
		if (currentImage != null) {
			currentImage.setSrc(currentPhoto.getPath());
			description.setValue(currentPhoto.getDescription());
			disableNavButton();
		}
		System.out.println("current photo id : " +currentPhoto.getId());	
	}
	
	public void disableNavButton() {
		if (count_photos > 1) {
			if (current_photo_id == 0) {
				firstButton.setDisabled(true);
				prevButton.setDisabled(true);
			} else {
				firstButton.setDisabled(false);
				prevButton.setDisabled(false);
			}
			if (current_photo_id == count_photos-1) {
				nextButton.setDisabled(true);
				lastButton.setDisabled(true);
			} else {
				nextButton.setDisabled(false);
				lastButton.setDisabled(false);
			}
		} else {
			firstButton.setDisabled(true);
			prevButton.setDisabled(true);
			nextButton.setDisabled(true);
			lastButton.setDisabled(true);
			randomButton.setDisabled(true);
		}
	}
	
	public String getAlbumName() {
		if (album != null) {
			return album.getName();		
		} else {
			return "";
		}
	}
	
	public Photo getCurrentPhoto() {
		return currentPhoto;
	}

	public void setCurrentPhoto(Photo currentPhoto) {
		this.currentPhoto = currentPhoto;
	}

	public int getCount_photos() {
		return count_photos;
	}

	public void setCount_photos(int count_photos) {
		this.count_photos = count_photos;
	}

	
	
}
